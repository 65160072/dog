/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.java.myapp;

/**
 *
 * @author informatics
 */
public class MainMenu extends javax.swing.JFrame {

    /**
     * Creates new form MainMenu
     */
    public MainMenu() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MenuBar = new javax.swing.JMenuBar();
        Database = new javax.swing.JMenu();
        MenuDB = new javax.swing.JMenuItem();
        MenuColl = new javax.swing.JMenuItem();
        MenuExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        allData = new javax.swing.JMenuItem();
        condition = new javax.swing.JMenuItem();
        Add = new javax.swing.JMenuItem();
        edit = new javax.swing.JMenuItem();
        delete = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("User Management System Development for BigC Company");

        Database.setText("ฐานข้อมูล");
        Database.setFont(new java.awt.Font("Microsoft Sans Serif", 0, 12)); // NOI18N

        MenuDB.setFont(new java.awt.Font("Microsoft Sans Serif", 0, 12)); // NOI18N
        MenuDB.setText("แสดงชื่อฐานข้อมูล");
        MenuDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuDBActionPerformed(evt);
            }
        });
        Database.add(MenuDB);

        MenuColl.setFont(new java.awt.Font("Microsoft Sans Serif", 0, 12)); // NOI18N
        MenuColl.setText("แสดงชื่อ Collection");
        MenuColl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuCollActionPerformed(evt);
            }
        });
        Database.add(MenuColl);

        MenuExit.setText("Exit");
        MenuExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuExitActionPerformed(evt);
            }
        });
        Database.add(MenuExit);

        MenuBar.add(Database);

        jMenu2.setText("จัดการข้อมูลผู้ใช้");
        jMenu2.setFont(new java.awt.Font("Microsoft Sans Serif", 0, 12)); // NOI18N

        allData.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        allData.setText("แสดงข้อมูลทั้งหมด");
        allData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allDataActionPerformed(evt);
            }
        });
        jMenu2.add(allData);

        condition.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        condition.setText("แสดงข้อมูลแบบมีเงื่อนไข");
        condition.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                conditionActionPerformed(evt);
            }
        });
        jMenu2.add(condition);

        Add.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        Add.setText("เพิ่มข้อมูล");
        Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddActionPerformed(evt);
            }
        });
        jMenu2.add(Add);

        edit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        edit.setText("แก้ไขข้อมูล");
        edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editActionPerformed(evt);
            }
        });
        jMenu2.add(edit);

        delete.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        delete.setText("ลบข้อมูล");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });
        jMenu2.add(delete);

        MenuBar.add(jMenu2);

        setJMenuBar(MenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 278, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void MenuDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuDBActionPerformed
        ShowDB showDB = new ShowDB();
        showDB.setVisible(true);
    }//GEN-LAST:event_MenuDBActionPerformed

    private void MenuCollActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuCollActionPerformed
        ShowCollection showColl = new ShowCollection();
        showColl.setVisible(true);
    }//GEN-LAST:event_MenuCollActionPerformed

    private void MenuExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuExitActionPerformed
        Login loginPage = new Login();
        loginPage.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_MenuExitActionPerformed

    private void allDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allDataActionPerformed
        ShowDocument showDcocument = new ShowDocument();
        showDcocument.setVisible(true);
    }//GEN-LAST:event_allDataActionPerformed

    private void conditionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_conditionActionPerformed
        ShowDocumentCondition showDocumentCondition = new ShowDocumentCondition();
        showDocumentCondition.setVisible(true);
    }//GEN-LAST:event_conditionActionPerformed

    private void AddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddActionPerformed
        AddDocument addDocument = new AddDocument();
        addDocument.setVisible(true);
    }//GEN-LAST:event_AddActionPerformed

    private void editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editActionPerformed
        EditDocument editDocument = new EditDocument();
        editDocument.setVisible(true);
    }//GEN-LAST:event_editActionPerformed

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
        DeletDocument deleteDocument = new DeletDocument();
        deleteDocument.setVisible(true);
    }//GEN-LAST:event_deleteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Add;
    private javax.swing.JMenu Database;
    private javax.swing.JMenuBar MenuBar;
    private javax.swing.JMenuItem MenuColl;
    private javax.swing.JMenuItem MenuDB;
    private javax.swing.JMenuItem MenuExit;
    private javax.swing.JMenuItem allData;
    private javax.swing.JMenuItem condition;
    private javax.swing.JMenuItem delete;
    private javax.swing.JMenuItem edit;
    private javax.swing.JMenu jMenu2;
    // End of variables declaration//GEN-END:variables
}
