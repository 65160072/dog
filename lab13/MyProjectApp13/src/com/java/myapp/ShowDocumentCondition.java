/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.java.myapp;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import javax.swing.DefaultListModel;
import com.mongodb.DBCursor;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author informatics
 */
public class ShowDocumentCondition extends javax.swing.JFrame {

    /**
     * Creates new form ShowDocument
     */
    public ShowDocumentCondition() {
        this.setTitle("Find Condition");
        initComponents();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtDoc = new javax.swing.JLabel();
        ShowDoc = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        ListFixDoc = new javax.swing.JList<>();
        Exit = new javax.swing.JButton();
        txt = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        ListDoc = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txtDoc.setFont(new java.awt.Font("Microsoft Sans Serif", 1, 24)); // NOI18N
        txtDoc.setText("แสดงข้อมูล Document ทั้งหมด");

        ShowDoc.setFont(new java.awt.Font("Microsoft Sans Serif", 1, 18)); // NOI18N
        ShowDoc.setText("แสดง Document");
        ShowDoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ShowDocActionPerformed(evt);
            }
        });

        ListFixDoc.setFont(new java.awt.Font("Microsoft Sans Serif", 0, 14)); // NOI18N
        jScrollPane2.setViewportView(ListFixDoc);

        Exit.setFont(new java.awt.Font("Microsoft Sans Serif", 1, 14)); // NOI18N
        Exit.setText("Exit");
        Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitActionPerformed(evt);
            }
        });

        txt.setFont(new java.awt.Font("Microsoft Sans Serif", 1, 18)); // NOI18N
        txt.setText(" ค้นหาแบบระบุเงื่อนไข   ระบุรหัส Employee  (emp_id) :");

        txtPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceActionPerformed(evt);
            }
        });

        btnSearch.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnSearch.setText("ค้นหา");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        ListDoc.setFont(new java.awt.Font("Microsoft Sans Serif", 0, 14)); // NOI18N
        jScrollPane3.setViewportView(ListDoc);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txt)
                        .addGap(18, 18, 18)
                        .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearch))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txtDoc)
                        .addComponent(ShowDoc)))
                .addContainerGap(120, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(Exit, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(350, 350, 350)
                    .addComponent(jScrollPane3)
                    .addGap(110, 110, 110)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Exit, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtDoc)
                        .addGap(27, 27, 27)
                        .addComponent(ShowDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(133, 133, 133)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearch))
                        .addGap(38, 38, 38)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(21, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(75, 75, 75)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(252, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ShowDocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ShowDocActionPerformed
        try {
            // เชื่อมต่อ MongoDB ที่ localhost พอร์ต 27017
            MongoClient mongo = new MongoClient("localhost", 27017);
            // เลือกใช้ฐานข้อมูลชื่อ "BigC"
            DB db = mongo.getDB("BigC");
            // เลือกใช้คอลเล็กชันชื่อ "Employee"
            DBCollection table = db.getCollection("Employee");
            // ค้นหาข้อมูลในคอลเล็กชัน
            DBCursor cursor = table.find();
            // สร้าง DefaultListModel เพื่อเก็บข้อมูลที่ค้นพบ
            DefaultListModel modeil = new DefaultListModel();
            // วนลูปผ่าน Cursor เพื่อเพิ่มข้อมูลลงใน DefaultListModel
            while (cursor.hasNext()) {
                modeil.addElement(cursor.next());
            }
            // กำหนด DefaultListModel ให้กับ ListDoc เพื่อแสดงผลข้อมูล
            ListDoc.setModel(modeil);
            // เพิ่ม ActionListener สำหรับ ListDoc เพื่อทำงานเมื่อผู้ใช้เลือกรายการ
            ListDoc.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent event) {
                    if (!event.getValueIsAdjusting()) {
                        // ดึงข้อมูลจาก ListDoc ที่ถูกเลือก
                        Object selectedData = ListDoc.getSelectedValue();
                        // แปลงข้อมูลเป็น BasicDBObject
                        BasicDBObject selectedDBObject = (BasicDBObject) selectedData;
                        // ดึงค่า empid จาก BasicDBObject
                        String empId = selectedDBObject.getString("emp_id");
                        // แสดงค่า empid ใน txtPrice
                        txtPrice.setText(empId);
                    }
                }
            });
        } catch (Exception e) {
        }
    }//GEN-LAST:event_ShowDocActionPerformed

    private void ExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitActionPerformed
        setVisible(false);
    }//GEN-LAST:event_ExitActionPerformed

    private void txtPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPriceActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        try {
            // เชื่อมต่อ MongoDB ที่ localhost พอร์ต 27017
            MongoClient mongo = new MongoClient("localhost", 27017);
            // เลือกใช้ฐานข้อมูลชื่อ "BigC"
            DB db = mongo.getDB("BigC");
            // ตรวจสอบว่า txtPrice ไม่ว่างเปล่าหรือไม่
            if (txtPrice.getText().isEmpty()) {
                // ถ้า txtPrice ว่างเปล่า แสดงข้อความแจ้งเตือน
                JOptionPane.showMessageDialog(this, "Please input data");
            } else {
                // ถ้า txtPrice ไม่ว่างเปล่า
                String empId = txtPrice.getText(); // รับค่า ID จาก txtPrice
                // เลือกใช้คอลเล็กชัน "Employee"
                DBCollection table = db.getCollection("Employee");
                // สร้างเงื่อนไขการค้นหาโดยใช้ ID ของพนักงาน
                BasicDBObject searchQuery = new BasicDBObject("emp_id", empId);
                // ค้นหาข้อมูลในคอลเล็กชันโดยใช้เงื่อนไขที่กำหนด
                DBCursor cursor = table.find(searchQuery);
                // สร้าง DefaultListModel เพื่อเก็บข้อมูลที่ค้นพบ
                DefaultListModel model = new DefaultListModel();
                // วนลูปผ่าน Cursor เพื่อเพิ่มข้อมูลลงใน DefaultListModel
                while (cursor.hasNext()) {
                    model.addElement(cursor.next());
                }
                // กำหนด DefaultListModel ให้กับ ListFixDoc เพื่อแสดงผลข้อมูลใน UI
                ListFixDoc.setModel(model);
            }
        } catch (Exception e) {
            // ในกรณีที่เกิดข้อผิดพลาดระหว่างการเชื่อมต่อหรือดำเนินการกับฐานข้อมูล MongoDB
            // สามารถจัดการข้อผิดพลาดได้ตามที่เหมาะสม
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ShowDocumentCondition.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ShowDocumentCondition.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ShowDocumentCondition.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ShowDocumentCondition.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ShowDocumentCondition().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Exit;
    private javax.swing.JList<String> ListDoc;
    private javax.swing.JList<String> ListFixDoc;
    private javax.swing.JButton ShowDoc;
    private javax.swing.JButton btnSearch;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel txt;
    private javax.swing.JLabel txtDoc;
    private javax.swing.JTextField txtPrice;
    // End of variables declaration//GEN-END:variables
}
